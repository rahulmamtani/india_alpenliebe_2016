<?php


function saveFeedback($service)
{
	echo "savefeedback";
	
	$userId = $_REQUEST['userId'];
    $eventId = $_REQUEST['eventId'];
	$feedbackResponse = $_REQUEST['optionSelected'];
	$other = htmlentities($_REQUEST['other']);
    
    $service->saveFeedback($userId,$eventId,$feedbackResponse,$other);
//	$result = $service->getEventsRespondedByUser($userId);
    
	header("Location:thanks.php");
}

function getEventsRespondedByUser($service)
{	

	$userId = $_REQUEST['userId'];
    $result = $service->getEventsRespondedByUser($userId);
    
	$output = "<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
				<plist version=\"1.0\">
				<array>
				    <dict>
				        <key>version</key>
				        <string>2</string>
				    </dict>
					<dict>
			           <key>let</key>
			           <string>eventsResponded=\"".$result."\"</string>           
			       </dict>
			    </array>
			</plist>";	
	
	echo $output;

}

function registerDevice($service)
{

	$uniqueId = $_REQUEST['uniqueId'];
	$name = $_REQUEST['txtName'];
	$email = $_REQUEST['txtEmail'];

		
	$service->registerDevice($name,$email,$uniqueId);
	
	$result = $service->checkDeviceExists($uniqueId);

	$output = "<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
				<plist version=\"1.0\">
				<array>
				    <dict>
				        <key>version</key>
				        <string>2</string>
				    </dict>
					<dict>
			           <key>let</key>
			           <string>device_reg_id=".$result."</string>           
			       </dict>
			    </array>
			</plist>";	
	echo $output;
	
	$script = "<script src='http://env1.blippar.com/BlipparUtils/BUtils1.0.1.js'></script><script>B.runCallApp('closeUserForm', ['".$result."']);</script>";
	
	echo $script;
	
}

function checkDeviceExistence($service)
{

	$uniqueId = $_REQUEST['uniqueId'];
	
	$result = $service->checkDeviceExists($uniqueId);	
	$id = $result;

	if(empty($id))
	{	
		echo " empty";
		$output = "<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
				<plist version=\"1.0\">
				<array>
				    <dict>
				        <key>version</key>
				        <string>2</string>
				    </dict>
					<dict>
			           <key>let</key>
			           <string>device_reg_id=0</string>           
			       </dict>
			    </array>
			</plist>";	
	}
	else
	{
		echo " not empty";
		$output = "<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
				<plist version=\"1.0\">
				<array>
				    <dict>
				        <key>version</key>
				        <string>2</string>
				    </dict>
					<dict>
			           <key>let</key>
			           <string>device_reg_id=".$id."</string>           
			       </dict>
			    </array>
			</plist>";

	}

	
	echo $output;
}


// execute commands

if(isset($_REQUEST['command']))
{
	
	$command = $_REQUEST['command'];

	$output = "";
	require_once 'service.php';
	$service = new Service();
	switch ($command) 
	{

	    case "checkDeviceExistence":
	        checkDeviceExistence($service);
	        break;

	    case "register":
	        registerDevice($service);
	        break;

	    case "saveFeedback":
	    	saveFeedback($service);        
	        break;
		case "getEventsData":
	    	getEventsRespondedByUser($service);        
	        break;	
			
	}

}

?>