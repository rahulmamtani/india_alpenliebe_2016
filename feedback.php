<?php

	require_once 'service.php';
	$service = new Service();
	$eventId = $_REQUEST['eventId'];
    $userId = $_REQUEST['userId'];	
	$result = $service->getEventFeedbackData($eventId,$userId);

	
	$totalVotes = intval($result[0]->option1Count) + intval($result[0]->option2Count) + intval($result[0]->option3Count) + intval($result[0]->option4Count);
	
//	echo "option1:".$result[0]->option1Count.",option2:".$result[0]->option2Count.",option3:".$result[0]->option3Count.",option4:".$result[0]->option4Count;

	$option1Pcnt = (intval($result[0]->option1Count) / intval($totalVotes)) * 100;
	$option2Pcnt = (intval($result[0]->option2Count) / intval($totalVotes))  * 100;
	$option3Pcnt = (intval($result[0]->option3Count) / intval($totalVotes))  * 100;
	$option4Pcnt = (intval($result[0]->option4Count) / intval($totalVotes))  * 100;
	

?>


<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>Feedback Form</title>
<link href="css/styles.css" rel="stylesheet" />
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>-->
<script src="jquery-1.11.0.min.js"></script>
<script src="http://env1.blippar.com/BlipparUtils/BUtils1.0.1.js"></script>
<style>
</style>
</head>
<body >
	<div class="wrapper">
        <div class="feedback" >
        	<div class="content">
            	
					<form name="feedbackForm" id="feedbackForm" action="commands.php" method="post" onsubmit="return (validateForm());">
						<div class="eventHeader">
							<div class="heading-text"><?php echo $result[0]->event_name ?></div>
							<div class="speaker-image"><img height="100%" width="100%" src="images/speakers/<?php echo $eventId ?>.png"/></div>
						</div>
						
						<div id="optionContent" style="margin:10px;">
							<a onclick="setValue(this,1)" style="cursor:pointer;">
								<div id="optionRow" class="option unselected">

									<div  class="optionText"><?php echo $result[0]->option1 ?> </div>
									<div class="voteMeter unselected" style="width:<?php echo $option1Pcnt?>%;"> </div>

								</div>
							</a>
							<a onclick="setValue(this,2)" style="cursor:pointer;">
								<div id="optionRow" class="option unselected">

									<div  class="optionText"><?php echo $result[0]->option2 ?> </div>
									<div class="voteMeter unselected" style="width:<?php echo $option2Pcnt?>%;"> </div>
								</div>
							</a>
							<a onclick="setValue(this,3)" style="cursor:pointer;">
								<div id="optionRow" class="option unselected">

									<div  class="optionText"><?php echo $result[0]->option3 ?> </div>
									<div class="voteMeter unselected" style="width:<?php echo $option3Pcnt?>%;"> </div>



								</div>
							</a>
							<a onclick="setValue(this,4)" style="cursor:pointer;">
								<div id="optionRow"  class="option unselected" style="cursor: pointer;">

									<div  class="optionText">Other</div>
									<div class="voteMeter unselected" style="width:<?php echo $option4Pcnt?>%;"> </div>

								</div>
							</a>
							<div style="padding-top:10px;display:none;" id="divOthers">
								<textarea rows="4" cols="51" name="other" id="other"></textarea>
							</div>
							<input type="hidden" name="optionSelected" id="optionSelected" value=""/>
							<input type="hidden" name="eventId" id="eventId" value="<?php echo $eventId ?>"/>
							<input type="hidden" name="userId" id="userId" value="<?php echo $userId ?>"/>
							<input type="hidden" name="command" id="command" value="saveFeedback"/>
						</div>


					</form>
				
            </div>
            <div class="close" id="exitBtn"><img src="images/close.png" /></div>
            <div class="submit" ><img src="images/btn_submit.png" id="submitform" /></div>
        </div>
    </div>
	<script language="javascript">

		$('#submitform').click(function(e) {

			e.preventDefault();
			$("#feedbackForm").submit();

		});
		

	
		function setValue(ele,optionValue) {
			
			$("#optionContent").find("a #optionRow").removeClass("selected");
			
			$("#optionContent").find("a #optionRow .voteMeter").removeClass("selected");
			
			$(ele).find("#optionRow").removeClass("unselected");
			$(ele).find("#optionRow").addClass("selected");
			$(ele).find("#optionRow .voteMeter").removeClass("unselected");
			$(ele).find("#optionRow .voteMeter").addClass("selected");
			
			document.getElementById("optionSelected").value = optionValue;

			if(optionValue!="4"){
				document.getElementById("divOthers").style.display = "none";
				document.getElementById("other").value = "";
			}
			else{
				document.getElementById("divOthers").style.display = document.getElementById("divOthers").style.display == "none" ? "" : "none";
				document.getElementById("other").value = document.getElementById("divOthers").style.display == "none" ? "" : document.getElementById("other").value;
				document.getElementById("other").focus();
				
			}
			

		}

		
		$("#exitBtn").click(function(e){

			B.runCallApp('call', ['closeFeedbackForm', '']);
//			setTimeout(function() {
//					Q(B.closeHTML());
//				}, 0);
			
		});

		function validateForm(){
//			!@#\$%\^\&*\)\(+=._-
			var re = /^[A-Za-z!@#\$%\&\^*\)\(+=._\-;,~'<> ]{3,100}$/;    
			var other = document.getElementById("other");

			if(document.getElementById("divOthers").style.display == ""){
				if(other.value.trim()==""){
					alert("Please provide a valid value for others.");
					other.focus();
					return false;
				}
			}
			else{
			
				if(document.getElementById("optionSelected").value == ""){

					alert('Please select one of the options.');
					return false;
				}

			}

			return true;

		}

	</script>	
</body>
</html>
