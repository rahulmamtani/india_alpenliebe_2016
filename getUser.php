<?php
	require_once "connection.php";
    $headers = array();
    $emailIDByDeviceID = "";
    $userExist = 0;

    foreach($_SERVER as $key => $value)
    {
        if (substr($key, 0, 5) <> 'HTTP_')
        {
            continue;
        }
        $header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
        $headers[$header] = $value;
    }

    $headers = array_change_key_case($headers, CASE_LOWER);
    
    if (isset($_REQUEST['uniqueID']))
        $uniqueID = $_REQUEST['uniqueID'];
    elseif (isset($headers["x-blippar-sessionid"]))
        $uniqueID = $headers["x-blippar-sessionid"];
    else
        $uniqueID = "";


    if($uniqueID != ""){
		$emailIDByDeviceID = $dbh->query("SELECT email FROM user_data WHERE deviceId = '".$uniqueID."' Order By createdOn DESC LIMIT 0 , 1")->fetchColumn();
    }else{
        echo "please provide uniqueID";
    }

    if($emailIDByDeviceID == ""){
        $userExist = 0;
    }else{
        $userExist = 1;
    }
?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
    <array>
        <dict>
            <key>version</key>
            <array>2</array>
        </dict>
        <dict>
           <key>let</key>
           <string>isUserExist = "<?= $userExist ?>" </string>
       </dict>
    </array>
</plist>