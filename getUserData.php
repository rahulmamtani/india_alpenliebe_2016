<?php
	require_once "connection.php";
    $headers = array();
    $cloudData = "";


    $result = $dbh->query("SELECT CONCAT_WS('~', name, email) AS userData FROM user_data ORDER BY createdOn DESC")->fetchAll();
    foreach($result as $row) {
        
        if($cloudData == ""){
            $cloudData = $row[userData];
        }else{
            $cloudData = $cloudData . "$" . $row[userData];
        }
    }
    echo $cloudData;  
?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
    <array>
        <dict>
            <key>version</key>
            <string>2</string>
        </dict>
        <dict>
           <key>let</key>
           <string>cloudData = "<?= $cloudData ?>" </string>
       </dict>
    </array>
</plist>