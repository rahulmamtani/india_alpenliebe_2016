<?php
	require_once "connection.php";
    $headers = array();
    $deviceId   = "";
    
    foreach($_SERVER as $key => $value)
    {
        if (substr($key, 0, 5) <> 'HTTP_')
        {
            continue;
        }
        $header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
        $headers[$header] = $value;
    }

    $headers = array_change_key_case($headers, CASE_LOWER);
    
    if(isset($_REQUEST['uniqueID']))
        $deviceId = $_REQUEST['uniqueID'];
    elseif (isset($headers["x-blippar-sessionid"]))
        $deviceId = $headers["x-blippar-sessionid"];
    else
        $deviceId = "";
	
	if(isset($_REQUEST['gameScore']))
        $newGameScore = $_REQUEST['gameScore'];
    else
        $newGameScore = 0;

    if(isset($_REQUEST['quizScore']))
        $newQuizScore = $_REQUEST['quizScore'];
    else
        $newQuizScore = 0;

    if($deviceId != "") {
            $gameScore = $dbh->query("SELECT gameScore FROM user_data WHERE deviceID = '".$deviceId."'")->fetchColumn();
            echo "gameScore:- ".$gameScore;
            $newGameScore = $newGameScore + $gameScore;

            $quizScore = $dbh->query("SELECT quizScore FROM user_data WHERE deviceID = '".$deviceId."'")->fetchColumn();
            echo "quizScore:- ".$quizScore;
            $newQuizScore = $newQuizScore + $quizScore;

            $sql = "UPDATE user_data SET gameScore = ".$newGameScore.", quizScore = ".$newQuizScore." WHERE deviceID = '" .$deviceId. "'";
            $sth = $dbh->prepare($sql);
            $sth->execute();
    }else{
        echo "please provide deviceId";
    }
?>